# serverless
Use serverlss framework to make lambda functions

## Install

### serverless framework
```
brew install serverless
```

### poetry if you use python
```
python -m pip install poetry
```

```
poetry config virtualenvs.in-project true
poetry config --list
```

### docker
Pull the 3rd party libary on docker which support Amazon Linux
Look over this document.

https://www.serverless.com/blog/serverless-python-packaging/

### AWS credentials
You need AWS credentials where you want to upload.

## Start your project from scrach

### Optional for Python

#### Start off your project

```
$ poetry new poetry-demo
$ cd poetry-demo
$ poetry install
```

or

```
$ serverless create \
  --template-url https://github.com/serverless/examples/tree/v3/aws-python
```

#### serverless-python-requirements
for packaging the python's 3rd party library

```
$ serverless plugin install -n serverless-python-requirements
```

#### pytest

```
$ poetry add -D pytest
$ poetry add -D pytest-cov
```

## Maitenance your project

```
$ npm outdated
$ poetry show --outdated
```

```
$ npm update
$ poetry update
```

## Serverless Policy
You can see these policy in the example under lslm directory.

- Log duration is 2 weeks
- Don't make s3 public

## Reference

### Serverless

- [Serverless.yml Reference](https://www.serverless.com/framework/docs/providers/aws/guide/serverless.yml)

- [Variables](https://www.serverless.com/framework/docs/providers/aws/guide/variables)
- [Variables aws spec](https://www.serverless.com/framework/docs/providers/aws/guide/variables#referencing-aws-specific-variables)


- [serverless / examples](https://github.com/serverless/examples)

### Poetry
- [Poetry](https://python-poetry.org/docs/)

- [Poetry はじめました - チーム開発における Python の開発環境](https://tech.isid.co.jp/entry/2022/08/22/Poetry_%E3%81%AF%E3%81%98%E3%82%81%E3%81%BE%E3%81%97%E3%81%9F_-_%E3%83%81%E3%83%BC%E3%83%A0%E9%96%8B%E7%99%BA%E3%81%AB%E3%81%8A%E3%81%91%E3%82%8B_Python_%E3%81%AE%E9%96%8B%E7%99%BA%E7%92%B0%E5%A2%83)