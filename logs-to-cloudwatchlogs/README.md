## requirements

```
$ npm install
$ poetry install
```

todo cloudwatch logs should be created with terraform

## xxx
todo later
This is example for you to show how serverless framework works.

This serverless framework just show hello world.


# Test

## Test with default options

(Default options are in `[tool.pytest.ini_options]` in the file `pyproject.toml`)

```
poetry run pytest
```

## Test with default and additional options

(This example additionally sets `--log-cli-level=DEBUG`)

```
poetry run pytest --log-cli-level=DEBUG
```

## Invocation
Don't invoke. Meaningless logs are sent to cloudwatch logs if you invoke.

```
$ serverless invoke local --function main
```

## Deploy

### production

```
$ serverless deploy --stage prod --aws-profile programboy
```
