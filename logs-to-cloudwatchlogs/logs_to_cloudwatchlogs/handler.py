import os
import json
import re
import logging
import urllib
import gzip
from urllib import response
from datetime import datetime

import boto3

from logs_to_cloudwatchlogs import list_tools


s3 = boto3.resource("s3")
cloud_watch_logs = boto3.client("logs")

LOG_GROUP_NAME = os.getenv('LOG_GROUP', "/aws/alb/log")
LOG_STREAM_NAME_PREFIX = os.getenv('LOG_STREAM_PREFIX', "alb-log-test_")

# logs to cloudwatch logs is about 2.0 times bigger than s3
COEFFICIENT = 2.5

# [ERROR] InvalidParameterException: An error occurred (InvalidParameterException)
# when calling the PutLogEvents operation: Upload too large: 1048916 bytes exceeds limit of 1048576
LOG_STREAM_MAX_BYTES = 1048576


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def main(event, context):

    event_time_str = event['Records'][0]['eventTime']
    event_time = int(datetime.strptime(event_time_str, '%Y-%m-%dT%H:%M:%S.%fZ').timestamp()) * 1000

    bucket_name = event['Records'][0]['s3']['bucket']['name']
    key_name = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')

    s3_object, s3_object_size = get_s3_object(bucket_name, key_name)

    if s3_object_size == 0:
        logger.info("The size of a s3 object is zero")
        return {"statusCode": 200}

    elb_listener_info = get_elb_listener_info(key_name)
    logs = parse_alb_log(s3_object, elb_listener_info)

    send_logs_to_cloudwatch(LOG_STREAM_NAME_PREFIX + key_name, logs, s3_object_size)

    response = {
        "statusCode": 200
    }

    return response


def get_s3_object(bucket_name: str, key_name: str):
    bucket = s3.Bucket(bucket_name)
    response_metadata = bucket.Object(key_name).get()
    response_bytes = gzip.decompress(response_metadata['Body'].read())
    response = response_bytes.decode('utf-8').splitlines()

    len_response_bytes = len(response_bytes)
    logger.info("JSON size of this object is {}".format(len_response_bytes))

    return response, len_response_bytes


def get_elb_listener_info(key):
    try:
        elb_listener_ip = key.split('_')[-2]
    except:
        elb_listener_ip = ''

    d = {}
    d["elb_listener_ip"] = elb_listener_ip

    return d

RE_TEXT = r"""
            ^(?P<type>[^ ]*)\u0020
            (?P<time>[^ ]*)\u0020
            (?P<elb>[^ ]*)\u0020
            (?P<client_ip>[^ ]*):(?P<client_port>[0-9]*)\u0020
            (?P<target_ip>[^ ]*)[:-](?P<target_port>[0-9]*)\u0020
            (?P<request_processing_time>[-.0-9]*)\u0020
            (?P<target_processing_time>[-.0-9]*)\u0020
            (?P<response_processing_time>[-.0-9]*)\u0020
            (?P<elb_status_code>|[-0-9]*)\u0020
            (?P<target_status_code>-|[-0-9]*)\u0020
            (?P<received_bytes>[-0-9]*)\u0020
            (?P<sent_bytes>[-0-9]*)\u0020
            \"(?P<request_method>[^ ]*)\u0020
            (?P<request_url>[^ ]*)\u0020
            (?P<request_http_version>- |[^ ]*)\"\u0020
            \"(?P<user_agent>[^\"]*)\"\u0020
            (?P<ssl_cipher>[A-Z0-9-]+)\u0020
            (?P<ssl_protocol>[A-Za-z0-9.-]*)\u0020
            (?P<target_group_arn>[^ ]*)\u0020
            \"(?P<trace_id>[^\"]*)\"\u0020
            \"(?P<domain_name>[^\"]*)\"\u0020
            \"(?P<chosen_cert_arn>[^\"]*)\"\u0020
            (?P<matched_rule_priority>[-.0-9]*)\u0020
            (?P<request_creation_time>[^ ]*)\u0020
            \"(?P<actions_executed>[^\"]*)\"\u0020
            \"(?P<redirect_url>[^\"]*)\"\u0020
            \"(?P<error_reason>[^\"]*)\"
            (?P<new_field>.*)
            """

RE_FORMAT = re.compile(RE_TEXT, flags=re.VERBOSE)

def parse_alb_log(s3_object, elb_info):
    l = []

    for c in s3_object:
        b = RE_FORMAT.match(c.rstrip("\n"))
        if b:
            a = b.groupdict()
            # timestamp
            a["timestamp"] = datetime.strptime(a["time"], "%Y-%m-%dT%H:%M:%S.%fZ").strftime('%Y-%m-%dT%H:%M:%S')
            # elb_listener_info
            a.update(elb_info)
            l.append(a)

    return l


def send_logs_to_cloudwatch(log_stream_name: str, logs: list, logs_size: int):
    """
    log group and its log streams are already build by terraform
    """

    def create_log_stream(log_stream_name: str):
        logger.info(log_stream_name)
        cloud_watch_logs.create_log_stream_response = cloud_watch_logs.create_log_stream(
                        logGroupName = LOG_GROUP_NAME,
                        logStreamName = log_stream_name)

    log_events = []

    for log in logs:
        t = int(datetime.strptime(log["time"], '%Y-%m-%dT%H:%M:%S.%fZ').timestamp()) * 1000
        logger.info(log)

        log_events.append({
            'timestamp': t,
            'message': json.dumps(log)
        })

    # logs must be sorted by choronogical
    # logs from s3 might not choronogical
    sorted_log_events = sorted(log_events, key = lambda x : x['timestamp'])

    divided_sorted_log_events = list_tools.split(sorted_log_events, logs_size * COEFFICIENT, LOG_STREAM_MAX_BYTES)

    for i, divided_logs in enumerate(divided_sorted_log_events):
        create_log_stream(log_stream_name + str(i))

        response = cloud_watch_logs.put_log_events(logGroupName=LOG_GROUP_NAME,
                                        logStreamName=log_stream_name + str(i),
                                        logEvents=divided_logs)
