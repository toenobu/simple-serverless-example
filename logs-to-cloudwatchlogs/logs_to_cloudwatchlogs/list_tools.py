import math

def split(arr, file_size, max_size):
    length = len(arr)

    n = math.ceil(file_size/max_size)
    s = math.floor(length/n)

    return splitBySize(arr, s)

def splitBySize(arr, size):
    arrs = []
    while len(arr) > size:
        pice = arr[:size]
        arrs.append(pice)
        arr   = arr[size:]
    arrs.append(arr)
    return arrs
