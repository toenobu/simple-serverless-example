
import os

os.environ['AWS_ACCESS_KEY_ID'] = 'testing'
os.environ['AWS_SECRET_ACCESS_KEY'] = 'testing'
os.environ['AWS_DEFAULT_REGION'] = 'ap-northeast-1'

from logs_to_cloudwatchlogs import handler

def test_no_meanging():
    assert 1 == 1


def test_parse_alb_log_normal():

    d = {}
    d["elb_listener_ip"] = '192.111.111.111'


    raws = []
    raws.append('https 2022-10-20T21:21:21.000588Z app/linkers-dev-alb/04e99fd7e5099dec 18.139.52.173:61219 10.10.3.246:80 0.009 0.045 0.000 200 200 363 53824 "GET https://inspection.linkers.net:443/ HTTP/1.1" "Datadog/Synthetics" ECDHE-RSA-AES128-GCM-SHA256 TLSv1.2 arn:aws:elasticloadbalancing:ap-northeast-1:290034026958:targetgroup/linkers-inspection-app-tg/8e9c67f813241885 "Root=1-63508e65-2301b12976f9773d46190e66" "inspection.linkers.net" "arn:aws:acm:ap-northeast-1:290034026958:certificate/ef121d35-0f90-44aa-8bd7-81ef8d42a4e8" 20 2022-10-19T23:55:17.456000Z "waf,forward" "-" "-" "10.10.3.246:80" "200" "-" "-"')
    raws.append('https 2022-10-19T23:58:17.111588Z app/linkers-dev-alb/04e99fd7e5099dec 18.139.52.173:61219 10.10.3.246:80 0.009 0.045 0.000 200 200 363 53824 "GET https://inspection.linkers.net:443/ HTTP/1.1" "Datadog/Synthetics" ECDHE-RSA-AES128-GCM-SHA256 TLSv1.2 arn:aws:elasticloadbalancing:ap-northeast-1:290034026958:targetgroup/linkers-inspection-app-tg/8e9c67f813241885 "Root=1-63508e65-2301b12976f9773d46190e66" "inspection.linkers.net" "arn:aws:acm:ap-northeast-1:290034026958:certificate/ef121d35-0f90-44aa-8bd7-81ef8d42a4e8" 20 2022-10-19T23:55:17.456000Z "waf,forward" "-" "-" "10.10.3.246:80" "200" "-" "-"')

    logs = handler.parse_alb_log(raws, d)

    assert logs[0]["type"] == "https"
    assert logs[0]["time"] == "2022-10-20T21:21:21.000588Z"
    assert logs[0]["elb"] == "app/linkers-dev-alb/04e99fd7e5099dec"
    assert logs[0]["client_ip"] == "18.139.52.173"
    assert logs[0]["client_port"] == "61219"
    assert logs[0]["target_ip"] == "10.10.3.246"
    assert logs[0]["target_port"] == "80"

    assert logs[0]["request_processing_time"] == "0.009"
    assert logs[0]["target_processing_time"] == "0.045"
    assert logs[0]["response_processing_time"] == "0.000"
    assert logs[0]["elb_status_code"] == "200"
    assert logs[0]["target_status_code"] == "200"

    assert logs[0]["received_bytes"] == "363"
    assert logs[0]["sent_bytes"] == "53824"

    assert logs[0]["request_method"] == "GET"
    assert logs[0]["request_url"] == "https://inspection.linkers.net:443/"
    assert logs[0]["request_http_version"] == "HTTP/1.1"
    assert logs[0]["user_agent"] == "Datadog/Synthetics"

    assert logs[0]["ssl_cipher"] == "ECDHE-RSA-AES128-GCM-SHA256"
    assert logs[0]["ssl_protocol"] == "TLSv1.2"
    assert logs[0]["target_group_arn"] == "arn:aws:elasticloadbalancing:ap-northeast-1:290034026958:targetgroup/linkers-inspection-app-tg/8e9c67f813241885"
    assert logs[0]["trace_id"] == "Root=1-63508e65-2301b12976f9773d46190e66"

    assert logs[0]["domain_name"] == "inspection.linkers.net"
    assert logs[0]["chosen_cert_arn"] == "arn:aws:acm:ap-northeast-1:290034026958:certificate/ef121d35-0f90-44aa-8bd7-81ef8d42a4e8"
    assert logs[0]["matched_rule_priority"] == "20"
    assert logs[0]["request_creation_time"] == "2022-10-19T23:55:17.456000Z"

    assert logs[0]["actions_executed"] == "waf,forward"
    assert logs[0]["redirect_url"] == "-"
    assert logs[0]["error_reason"] == "-"

    assert logs[1]["type"] == "https"
    assert logs[1]["time"] == "2022-10-19T23:58:17.111588Z"

