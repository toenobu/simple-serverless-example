.PHONY: virtualenv clean activate deactivate deps

VENV_DIRECTORY = venv
MID_TERM = mid-term
FINAL_TERM = final-term

VENV_DIRECTORY_MAC = venvMAC

virtualenv:
	python3 -m venv $(VENV_DIRECTORY)

clean:
	echo "Try 'rm -fr $(VENV_DIRECTORY)'"

activate:
	echo "Try 'source $(VENV_DIRECTORY)/bin/activate'"

deactivate:
	echo "Try 'deactivate'"

deps:
	pip install -r requirements.txt
	pip install -r requirements.dev.txt

notebook:
	jupyter notebook

notebook-lab:
	jupyter-lab

format:
	black main.py proc

inspect:
	mypy main.py proc

assert-seerrter:
	python asserter.py

serve:
	python3 -m http.server 8081

lima-start:
	limactl start simple-svless
	docker context use simple-svless

lima-shell:
	limactl shell simple-svless

lima-stop:
	limactl stop simple-svless

install:
	python -m pip install poetry
	poetry config virtualenvs.in-project true
	poetry config --list
