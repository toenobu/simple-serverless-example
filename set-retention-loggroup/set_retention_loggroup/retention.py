import sys
import boto3

from typing import List

logs = boto3.client('logs')


def set_retention(log_group_names: List, retention_days = 365):
    """
    set retention policy on the given log_group_names.
    """

    for log_group_name in log_group_names:

        try:
            logs.put_retention_policy(
                logGroupName=log_group_name, retentionInDays=retention_days
            )

            print(f"Retention policy set to {retention_days} days for log group '{log_group_name}'.")
        except logs.exceptions.ResourceNotFoundException:
            print(f"Log group '{log_group_name}' does not exist.")
        except Exception as e:
            print(f"An error occurred: {e}")


if __name__ == '__main__':
    if len(sys.argv) < 2:
        raise Exception(f'Set log groups like foo_log,bar_log')

    groups = sys.argv[1].split(",")

    set_retention(groups, 365)