import sys

from retention import set_retention

if __name__ == '__main__':
    if len(sys.argv) < 2:
        raise Exception(f'Set log groups like foo_log,bar_log')

    groups = sys.argv[1].split(",")

    set_retention(groups, 365)
