## Set retention policy on the log groups
Set retention policy on the log groups.

## requirements

```console 
poetry install
```

## Test

### Test with default options

Not test yet... 

## Invoke local

```console 
AWS_PROFILE=programboy poetry run python set_retention_loggroup/main.py foo_log,bar_log...
```

## Build local

```
docker build -t log_group_retention .
```

## Invoke in container 

```
docker run -v ~/.aws:/root/.aws -e AWS_PROFILE=programboy -it --rm log_group_retention foo_log,bar_log...
```
