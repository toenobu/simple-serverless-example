from example import handler

def test_make_body():

    body = handler.make_body({})

    assert body["message"] == "Go Serverless v1.0! Your function executed successfully!"