### Make a new vm

```
limactl start --name=simple-svless lima.yaml
```

if you want to more about lima, go to memos in my dropbox.

```
cpus: 2
memory: 2GiB
disk: 2GiB
```

```
mounts:
- location: "~"
  writable: true
```

### login to vm

```
limactl shell simple-svless
```

### docker context

```
docker context create simple-svless --docker "host=unix:///Users/toenobu/.lima/simple-svless/sock/docker.sock"
```

### delete system journal

ジャーナルファイルを削除したい場合・journalctl
https://linux.just4fun.biz/?Linux%E7%92%B0%E5%A2%83%E8%A8%AD%E5%AE%9A/%E3%82%B8%E3%83%A3%E3%83%BC%E3%83%8A%E3%83%AB%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB%E3%82%92%E5%89%8A%E9%99%A4%E3%81%97%E3%81%9F%E3%81%84%E5%A0%B4%E5%90%88%E3%83%BBjournalctl